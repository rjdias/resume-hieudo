# Container to compile the hieudo-build latex template
OCI container format to build a curriculum using the hieudo-build latex template.  


## Contents:
* Dockerfile.
* Entry-point script.
* [GitLab CI file][gitlab-ci.yml].


## How to use:
* Copy the example `main_example.tex` into `main.tex`.  
  (`cp compile/main_example.tex compile/main.tex`)
* Edit `main.tex` to your preference.
* Build the container (see `How to build`).
* Run the container (see `How to run`).


## How to build:
### Automatically with Docker:
Fork this repository into your own.  
There's a [GitLab CI file][gitlab-ci.yml] that will build the container and push it into the provided [GitLab Container Registry][container_registry].  
The pipeline is just to build the container image.  
To build the document I would advise you to do it locally, using the container image built by the CI.  
The pipeline is configured to start only after a button push.  

### Manually with Docker:
```bash
CI_REGISTRY_IMAGE="registry.gitlab.com/rjdias/resume-hieudo"
DOCKER_TAG_LATEST="${CI_REGISTRY_IMAGE}:latest"

docker build -t ${DOCKER_TAG_LATEST} .
```


## How to run:
### Docker run:
```bash
CI_REGISTRY_IMAGE="registry.gitlab.com/rjdias/resume-hieudo"
DOCKER_TAG_LATEST="${CI_REGISTRY_IMAGE}:latest"

docker login registry.gitlab.com

docker run --rm -it \
-v "$(pwd)/compile":/tmp/compile:Z \
--user $(id -u):$(id -g) \
${DOCKER_TAG_LATEST}
```


## Resources:
[Docker Reference documentation](https://docs.docker.com/reference/)  
[Yet Another Resume Template](https://www.overleaf.com/latex/templates/yet-another-resume-template/gdxwyyqhspsf)

[container_registry]: (https://docs.gitlab.com/ce/user/project/container_registry.html)
[gitlab-ci.yml]: .gitlab-ci.yml
