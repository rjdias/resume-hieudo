#!/bin/bash
XELATEX_RUN="${XELATEX_OPTS} -jobname=${JOB_NAME} -output-directory=${BUILD_DIR} ${TEX_FILE}"
echo -e "::: running as '$(id -u):$(id -g)' :::\n::: current dir '$(pwd)' :::";
echo -e "::: mkdir '${BUILD_DIR}' as build dir :::"; mkdir -p ${BUILD_DIR};
echo -e "::: xelatex ${XELATEX_RUN} :::";

xelatex ${XELATEX_RUN}

echo -e "::: cp '${BUILD_DIR}/${JOB_NAME}.pdf' to '$(pwd)' :::"
cp ${BUILD_DIR}/${JOB_NAME}.pdf .

