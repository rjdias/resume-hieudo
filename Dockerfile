FROM registry.fedoraproject.org/fedora-minimal:latest

LABEL maintainer="José Dias <121466-rjdias@users.noreply.gitlab.com>"

ENV \
NEW_UID=1000 \
TEX_FILE="main.tex" \
JOB_NAME="output" \
BUILD_DIR="build" \
XELATEX_OPTS="-interaction=nonstopmode"

COPY ["compile.sh", "/tmp/compile.sh"]

RUN \
set -eux; \
useradd -u $NEW_UID latex; \
sed -i 's/enabled=1/enabled=0/' /etc/yum.repos.d/fedora-modular.repo; \
sed -i 's/enabled=1/enabled=0/' /etc/yum.repos.d/fedora-updates-modular.repo; \
chmod 777 /tmp/compile.sh && \
\
microdnf -y update; \
\
microdnf -y install \
texlive-babel-english \
texlive-datetime2 \
texlive-datetime2-english \
texlive-euenc \
texlive-fontawesome \
texlive-isodate \
texlive-textpos \
texlive-titlesec \
texlive-xetex \
texlive-xltxtra && \
\
microdnf clean all && \
\
xelatex -version

VOLUME ["/tmp/compile"]
WORKDIR /tmp/compile
USER latex
ENTRYPOINT ["/tmp/compile.sh"]

